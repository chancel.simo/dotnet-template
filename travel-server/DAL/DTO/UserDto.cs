
namespace TravelAPI.DAL.DTO;

public class UserDto
{
    public int ID { get; set; }
    public string LastName { get; set; } = string.Empty;

    public string FirstName { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;

    public string FullName { get; set; } = string.Empty;

    public string? AccessToken { get; set; }
}