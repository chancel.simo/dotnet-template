using Microsoft.EntityFrameworkCore;
using TravelAPI.DAL.Models;
using TravelAPI.Config;

namespace TravelAPI.DAL;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options) { }

    public DbSet<User> UsersDbSet { get; set; }

    public static ILoggerFactory MyLoggerFactory;
    protected void ConfigureForMysql(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = AppSettings.MySqlConnectionString;
        optionsBuilder.UseMySql(connectionString, MySqlServerVersion.AutoDetect(connectionString),
        x =>
        {
            x.CommandTimeout(60);
        });
        optionsBuilder.EnableSensitiveDataLogging(true);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        ConfigureForMysql(optionsBuilder);
    }
}