
using System.ComponentModel.DataAnnotations.Schema;
using TravelAPI.DAL.DTO;

namespace TravelAPI.DAL.Models;

[Table("users")]
public class User
{
    public int ID { get; set; }
    public string LastName { get; set; } = string.Empty;

    public string FirstName { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public byte[] PasswordHash { get; set; }
    public byte[] PasswordSalt { get; set; }

    public UserDto GetDto()
    {
        return new UserDto()
        {
            ID = ID,
            Email = Email,
            LastName = LastName,
            FirstName = FirstName,
            FullName = LastName + FirstName,
        };
    }
}
