using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TravelAPI.DAL;
using TravelAPI.DAL.Models;
using TravelAPI.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using TravelAPI.Services;
using System.Security.Claims;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace TravelAPI.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    public readonly DataContext CurrentContext;

    public UserController(DataContext context)
    {
        CurrentContext = context;
    }


    [HttpGet]
    [SwaggerOperation("GetConnectedUser")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public async Task<ActionResult<List<User>>> GetUser()
    {
        var user = GetCurrentUser();
        return Ok(await CurrentContext.UsersDbSet.ToListAsync());
    }

    [HttpPost("register")]
    [SwaggerOperation("Register")]
    public async Task<ActionResult<UserResponse>> Register([FromBody] UserDto request)
    {
        return Ok(UsersService.Register(CurrentContext, request));
    }

    [HttpPost("login")]
    [SwaggerOperation("Login")]
    [SwaggerResponse(200, "LoginResponse", typeof(UserResponse))]
    public async Task<ActionResult<UserResponse>> Login([FromBody] LoginRequest request)
    {
        return Ok(UsersService.Login(CurrentContext, request));
    }

    private UserDto? GetCurrentUser()
    {
        var identity = HttpContext.User.Identity as ClaimsIdentity;
        if (identity != null)
        {
            var userClaims = identity.Claims;
            var parseUserDto = userClaims.FirstOrDefault(x => x.Type == ClaimTypes.UserData)?.Value;

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
            };

            var user = JsonConvert.DeserializeObject<UserDto>(parseUserDto, settings);

            return user;
        }

        return null;
    }

}