using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.DependencyInjection;
using TravelAPI.DAL;
using System.Text;
using Microsoft.OpenApi.Models;
using TravelAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using TravelAPI;
using Swashbuckle.AspNetCore.Filters;
using TravelAPI.Config;
using TravelAPI.Utils.Swagger;


var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

ConfigureServices(builder.Services, builder.Configuration, builder.Environment.IsDevelopment());

var app = builder.Build();

Configure(app, builder.Environment);

app.Run();

void ConfigureServices(IServiceCollection services, ConfigurationManager configuration, bool IsDevelopment)
{
    var connectionString = configuration.GetConnectionString("MySqlConnectionString");
    services.AddDbContext<DataContext>(options => options.UseMySql(MySqlServerVersion.AutoDetect(connectionString)));

    services.AddControllers().AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
    });
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    //builder.Services.AddSwaggerGen();

    var origins = new List<string> { AppSettings.ClientBaseUrl };
    services.AddCors(options =>
    {
        options.AddPolicy("MyPolicy",
            builder =>
                        {
                            builder.AllowAnyHeader();
                            builder.AllowAnyMethod();
                            builder.AllowCredentials();
                            builder.WithOrigins(origins.ToArray());
                        }
        );
    });

    services.AddSwaggerGen(options =>
    {
        options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
            Description = "Standard Authorization Bearer",
            In = ParameterLocation.Header,
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey
        });

        options.OperationFilter<SecurityRequirementsOperationFilter>();
    });
    services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
       .AddJwtBearer(options =>
       {

           var tokenSecretKey = AppSettings.TokenSecretKey;

           options.TokenValidationParameters = new TokenValidationParameters
           {
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                   .GetBytes(tokenSecretKey)
               ),
               ValidateIssuer = false,
               ValidateAudience = false,
           };
       });
    services.AddDistributedMemoryCache();
    services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();

    services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromMinutes(30);
        options.Cookie.HttpOnly = true;
        options.Cookie.Name = "Travel-AspNetCoreSession";
    });

    services.AddMemoryCache();

    if (IsDevelopment)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v" + 1, new OpenApiInfo { Title = AppSettings.APITitle, Version = "v" + 1 });
            c.OperationFilter<SwaggerOperationNameFilter>();
            c.SchemaFilter<ModifySchemaFilter>();
            c.EnableAnnotations();
        });

        services.AddSwaggerGenNewtonsoftSupport();

    }
}

void Configure(WebApplication app, IWebHostEnvironment env)
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint(AppSettings.SwaggerEndPoint, AppSettings.APITitle);
        });
    }

    app.UseCors("MyPolicy");

    var origins = new List<string>{
        AppSettings.ClientBaseUrl
    };

    app.UseSession();
    app.UseStaticFiles();
    app.UseHttpsRedirection();
    app.UseAuthentication();

    app.MapControllers();

    app.UseRouting();

    app.UseAuthorization();

    app.UseEndpoints(endpoint =>
    {
        endpoint.MapControllers();

        if (app.Environment.IsDevelopment())
        {
            endpoint.MapGet("/", context =>
            {
                context.Response.Redirect("/swagger");
                return Task.FromResult(0);
            });
        }
        else
            endpoint.MapGet("/", (context) => context.Response.WriteAsync("API OK"));

    });
}

