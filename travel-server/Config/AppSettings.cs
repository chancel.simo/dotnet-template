
namespace TravelAPI.Config;

public static class AppSettings
{
    public static string ConnectionString = "Data Source=localhost\\sqlexpress;Initial Catalog=travel_db;Integrated Security=True;MultipleActiveResultSets=true";
    public static string MySqlConnectionString = "server=localhost;port=3306;user=root;password=root;database=travel_db";

    public static string TokenSecretKey = "this is my custom Secret key for authentication";

    public static string SwaggerEndPoint = $"/swagger/v{1}/swagger.json";

    public static string APITitle = "TRAVEL API";

    public static bool IsDev { get; set; }

    public static bool IsProd { get; set; }

    public static string ClientBaseUrl = "http://localhost:4200";

}
