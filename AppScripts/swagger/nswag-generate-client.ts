import * as path from 'path';
import * as fs from 'fs';
import * as colors from 'colors/safe';
import { execSync } from 'child_process';

const apiSwaggerUrl = 'http://localhost:5022/swagger/v1/swagger.json';
const swaggerCodegenConfig = path.join(__dirname, 'swagger-codegen-config.json');
const swaggerFrontOutput = path.join(__dirname, '..', '..', 'travel-config', 'src', 'services', 'api-client.generated');

if (!fs.existsSync(swaggerCodegenConfig)) {
    console.error(colors.red(`Impossible de générer le code client : le fichier de configuration ${swaggerCodegenConfig} n'existe pas !`));
    process.exit(1);
}

try {
    execSync(`npx openapi-generator-cli generate -i "${apiSwaggerUrl}" -g typescript-angular -c "${swaggerCodegenConfig}" -o "${swaggerFrontOutput}" --type-mappings DateTime=Date`).toString();
    console.log(colors.green(`Code client généré avec succès !`));

} catch (error) {
    console.error(colors.red(`Impossible de générer le code client : ${error}`));

}
    //NodeHelpers.executeCommand(`java -jar "${swaggerCodegenBin}" generate -i "${apiSwaggerUrl}" -g typescript-angular -c "${swaggerCodegenConfig}" -o "${swaggerFrontOutput}" --type-mappings Date=Date`,
