import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import jwt_decode from "jwt-decode";
import { AuthData } from "../global/auth-data";
import { UserDto } from "../services/api-client.generated";


@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    constructor(
        private _router: Router,
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        this.refreshAuthTokenAccess();
        if (!AuthData.isConnected) {
            this._router.navigate(['/login']);
            return false;
        }
        return true;
    }

    private refreshAuthTokenAccess() {
        const access_token = localStorage.getItem('access_token');
        if (access_token) {
            AuthData.isConnected = true;
            AuthData.currentUser = jwt_decode(access_token) as UserDto;
            console.log("🚀 ~ refreshAuthTokenAccess ~ AuthData.currentUser", AuthData.currentUser);
        }
        else AuthData.isConnected = false;
    }
}