import { Directive } from "@angular/core";
import { AuthData } from "../../global/auth-data";

@Directive({})
export abstract class BaseComponent {
    public AuthData = AuthData;
}