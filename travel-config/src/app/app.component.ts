import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: [`
    app-root{
      height:100%;
      width:100%;
    }
  `]
})
export class AppComponent {
  title = 'travel-config';
}
