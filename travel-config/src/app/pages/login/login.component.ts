import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LoginRequest, UserService } from '../../../services/api-client.generated';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../../base/base.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  request: LoginRequest = {};
  loading: boolean = false;
  messageApi?: string;
  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  async onLogin() {
    this.loading = true;

    const loginResponse = await this.userService.login({ Email: this.request.Email, Password: this.request.Password }).toPromise();
    console.log("🚀 ~ onLogin ~ loginResponse", loginResponse)
    this.loading = false;
    if (!loginResponse.Success) {
      this.messageApi = loginResponse.Message as string;
      return;
    }

    localStorage.setItem('access_token', loginResponse.User?.AccessToken!);
    this.router.navigate(['/home']);
  }

}
