import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BASE_PATH } from '../services/api-client.generated';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { CustomInterceptor } from '../services/http-interceptor';
import { AuthGuardService } from '../guards/guard.service';
import { MainDrawerModule } from './components/main-drawer/main-drawer.module';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MainDrawerModule,
  ],
  providers: [
    { provide: BASE_PATH, useValue: environment.ApiBaseUrl },
    { provide: HTTP_INTERCEPTORS, useClass: CustomInterceptor, multi: true },
    AuthGuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
