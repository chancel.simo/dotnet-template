export enum RouteList {
    HOME = "home",
    TRAJET = "trajet",
    LOGIN = "login",
    COMPTA = "compta",
    SETTING = "setting"
}