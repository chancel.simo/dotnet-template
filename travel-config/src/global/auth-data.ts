import { UserDto } from "../services/api-client.generated";


export class AuthData {

    public static currentUser: UserDto;
    public static isConnected: boolean = false;
}